from abc import abstractmethod, ABC
from copy import deepcopy


class GameState(ABC):
    _board: list[str]
    _next_player: str
    _max_pieces: int

    def __repr__(self):
        return f'{"".join(self._board)}{self._next_player}'

    def pieces(self):
        return sum(1 for pos in self._board if pos != '0')

    def player(self):
        return self._next_player

    @abstractmethod
    def transformations(self):
        return self,

    def transfold(self):
        """
        transform + fold = transfold

        :return: transformed self with least lexicographical representation (lex repr)
        """
        return min(self.transformations(), key=lambda s: repr(s))

    def __hash__(self):
        return int(repr(self.transfold()))

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return hash(self) == hash(other)
        return False

    def eq_board(self, other):
        if isinstance(other, type(self)):
            return self.transfold()._board == other.transfold()._board
        return False

    @abstractmethod
    def winner(self) -> str:
        pass

    def next_states(self):
        if self.winner() != '0':
            return {self}  # to make win states absorbing
        states = set()
        if self._next_player == '1':
            next_next_player = '2'
        else:
            next_next_player = '1'
        if self.pieces() < self._max_pieces:
            for index, piece in enumerate(self._board):
                if piece == '0':
                    new = deepcopy(self)
                    new._board[index] = self._next_player
                    new._next_player = next_next_player
                    states.add(new)
        else:
            for index, piece in enumerate(self._board):
                if piece == self._next_player:
                    for adjacent in type(self)._adjacent(index):
                        if self._board[adjacent] == '0':
                            new = deepcopy(self)
                            new._board[adjacent] = self._next_player
                            new._board[index] = '0'
                            new._next_player = next_next_player
                            states.add(new)
        return states

    @staticmethod
    @abstractmethod
    def _adjacent(index) -> tuple:
        pass
