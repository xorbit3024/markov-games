from abc import ABC
from copy import deepcopy

from states.game_state import GameState


class Square3State(GameState, ABC):
    """
    index   position
    ------------------
    0       top left
    1       top mid
    2       top right
    3       mid left
    4       mid mid
    5       mid right
    6       bot left
    7       bot mid
    8       bot right

    element piece
    ------------------
    '0'     empty
    '1'     player 1
    '2'     player 2
    """

    def __str__(self):
        return (f'Next: {self._next_player}\n'
                f'{"".join(self._board[0:3])}\n'
                f'{"".join(self._board[3:6])}\n'
                f'{"".join(self._board[6:9])}'
                )

    def winner(self):
        """
        :return: '0' if no winners, else '#' of the player that won
        """
        for player in ['1', '2']:
            if any((all(piece == player for piece in self._board[0:3]),
                    all(piece == player for piece in self._board[3:6]),
                    all(piece == player for piece in self._board[6:9]),
                    all(piece == player for piece in self._board[0:7:3]),
                    all(piece == player for piece in self._board[1:8:3]),
                    all(piece == player for piece in self._board[2:9:3]),
                    all(piece == player for piece in self._board[0:9:4]),
                    all(piece == player for piece in self._board[2:7:2]),
                    )):
                return player
        return '0'

    def __rotate90(self):
        new = deepcopy(self)
        new._board[0] = self._board[2]
        new._board[1] = self._board[5]
        new._board[2] = self._board[8]
        new._board[3] = self._board[1]
        new._board[5] = self._board[7]
        new._board[6] = self._board[0]
        new._board[7] = self._board[3]
        new._board[8] = self._board[6]
        return new

    def __reflect_main(self):
        new = deepcopy(self)
        new._board[1] = self._board[3]
        new._board[2] = self._board[6]
        new._board[3] = self._board[1]
        new._board[5] = self._board[7]
        new._board[6] = self._board[2]
        new._board[7] = self._board[5]
        return new

    def transformations(self):
        result = [self, self.__reflect_main()]
        for _ in range(6):
            result.append(result[-2].__rotate90())
        return result
