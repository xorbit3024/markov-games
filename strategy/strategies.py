from functools import cache

from graphs.graph import Graph
from tapatan.taptan_state import TapatanState


def random(g: Graph, vertices, player: str):
    """
    Randomly picks a move from all legal moves.

    :param g: Graph of states and moves
    :param vertices: ORDERED list of states to construct transition matrix with
    :param player: '1' or '2'
    :return: Transition matrix with all of player's moves
    """
    key = {v: i for i, v in enumerate(vertices)}
    a = g.adjacency_matrix(key)
    for i, row in enumerate(a):
        if vertices[i].player() == player:
            row_sum = sum(row)
            for j in range(len(row)):
                row[j] /= row_sum
        else:
            a[i] = [0] * len(a[i])
    return a


def minimax(g: Graph, vertices, player: str, depth=1, eval_fn=None):
    """
    Uses minimax to select best moves, then randomly selects from the best.
    Minimax assumes opponent is using same strategy. Moves will not be optimal otherwise.

    :param g: Graph of states and moves
    :param vertices: ORDERED list of states to construct transition matrix with
    :param player: '1' or '2'
    :param depth: POSITIVE INTEGER number of moves ahead to look
    :param eval_fn: Function of game states to float. Higher values
                    indicate more a valuable states for that player.
    :return: Transition matrix of player's moves
    """

    if depth < 1:
        raise ValueError('Depth must be positive integer.')

    def default_eval(state):
        if state.winner() == '0':
            return 0.0
        elif state.winner() == player:
            return 1.0
        else:
            return -1.0

    if eval_fn is None:
        eval_fn = default_eval

    @cache
    def value(state, level):
        if level == depth:
            return eval_fn(state)
        elif level % 2 == 1:
            result = min(value(v, level + 1) if v.winner() == '0' else eval_fn(v) for v in g.neighbors(state))
            return result
        else:
            result = max(value(v, level + 1) if v.winner() == '0' else eval_fn(v) for v in g.neighbors(state))
            return result

    key = {v: i for i, v in enumerate(vertices)}
    a = [[0.0] * g.n() for _ in range(g.n())]
    for i, row in enumerate(a):
        u = vertices[i]
        if u.player() == player:
            moves = {v: value(v, 1) for v in g.neighbors(u)}
            max_value = max(moves.values())
            best_moves = [v for v in moves.keys() if moves[v] == max_value]
            for move in best_moves:
                row[key[move]] = 1 / len(best_moves)
    return a
