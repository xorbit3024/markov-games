import inspect
import os
import pickle
import sys
from collections import Counter
from copy import deepcopy, copy
from functools import cache

import networkx as nx
import numpy as np
from numpy import transpose, concatenate
from pyvis.network import Network
from scipy.sparse import bsr_matrix, spmatrix
from scipy.sparse.linalg import aslinearoperator

from generalizations.picaria_hex_state import PicariaHexState
from generalizations.tapatan_5 import Tapatan5State
from generalizations.tapatan_6 import Tapatan6State
from generalizations.tapatan_repeat_draw_state import TapatanRepeatDrawState
from graphs.search import dfs, scc, bfs, bfs_distances
from picaria.picaria_state import PicariaState
from states.game_state import GameState
from strategy.strategies import random, minimax
from tapatan.taptan_state import TapatanState
from graphs.graph import Graph


def main():
    analyze_mc()


def prune_distance(g: Graph, s, d: int):
    """
    removes vertices and corresponding edges from g that are more than d away from s

    :param g: graph to prune
    :param s: start vertex
    :param d: max distance to keep
    :return: pruned copy of graph
    """
    distances = bfs_distances(g, s)
    h = deepcopy(g)
    for k, v in distances.items():
        if v > d:
            h.remove_vertex(k)
    return h


def transform_readable(g: Graph, start: GameState):
    h = Graph()
    visited = set()
    fringe = [start]
    while len(fringe) > 0:
        u = fringe.pop(0)
        if u not in visited:
            visited.add(u)
            h.add_vertex(u)
            for v in g.neighbors(u.transfold()):
                if v not in h.vertices():
                    valid = u.next_states()
                    w = v
                    for w in v.transformations():
                        if any(repr(w) == repr(x) for x in valid):
                            break
                    h.add_edge(u, w)
                    if w not in visited:
                        fringe.append(w)
                else:
                    vertices = list(h.vertices())
                    h.add_edge(u, vertices[vertices.index(v)])
    return h


def analyze_mc():
    game = get_game()
    g: Graph = load(f'{game}/{game}.graph')

    # canonical sorting
    vertices = g.vertices()
    vertices = sorted(vertices, key=lambda u: hash(u))
    vertices = sorted(vertices, key=lambda u: u.pieces())
    vertices = sorted(vertices, key=lambda u: u.winner())

    p1_moves = get_moves('1', game, vertices)
    p2_moves = get_moves('2', game, vertices)

    p = np.array(p1_moves) + np.array(p2_moves)
    t = sum(1 for u in vertices if u.winner() == '0')
    q = p[:t, :t]
    r = p[:t, t:]
    n = np.linalg.inv(np.identity(q.shape[0]) - q)
    b = n.dot(r)

    # printing indices.csv
    folder_name = input('Enter folder name to save data in: ')
    if not os.path.isdir(f'{game}/mc_data/{folder_name}'):
        os.mkdir(f'{game}/mc_data/{folder_name}')
    with open(f'{game}/mc_data/{folder_name}/indices.csv', 'wt') as outfile:
        print('"index","repr","str"', file=outfile)
        for i in range(len(vertices)):
            print(f'"{i}","{repr(vertices[i])}","{str(vertices[i])}"', file=outfile)

    # index appending
    # IF YOU DONT WANT INDICES IN THE CSV, COMMENT OUT THIS BLOCK
    p = np.concatenate((np.arange(p.shape[0]).reshape(p.shape[0], 1), p), axis=1)
    p = np.concatenate((np.arange(-1, p.shape[1] - 1).reshape(1, p.shape[1]), p), axis=0)
    q = np.concatenate((np.arange(q.shape[0]).reshape(q.shape[0], 1), q), axis=1)
    q = np.concatenate((np.arange(-1, q.shape[1] - 1).reshape(1, q.shape[1]), q), axis=0)
    r = np.concatenate((np.arange(r.shape[0]).reshape(r.shape[0], 1), r), axis=1)
    r = np.concatenate((np.arange(t - 1, r.shape[1] + t - 1).reshape(1, r.shape[1]), r), axis=0)
    n = np.concatenate((np.arange(n.shape[0]).reshape(n.shape[0], 1), n), axis=1)
    n = np.concatenate((np.arange(-1, n.shape[1] - 1).reshape(1, n.shape[1]), n), axis=0)
    b = np.concatenate((np.arange(b.shape[0]).reshape(b.shape[0], 1), b), axis=1)
    b = np.concatenate((np.arange(t - 1, b.shape[1] + t - 1).reshape(1, b.shape[1]), b), axis=0)

    # printing csvs
    np.savetxt(f'{game}/mc_data/{folder_name}/p.csv', p, delimiter=',')
    np.savetxt(f'{game}/mc_data/{folder_name}/q.csv', q, delimiter=',')
    np.savetxt(f'{game}/mc_data/{folder_name}/r.csv', r, delimiter=',')
    np.savetxt(f'{game}/mc_data/{folder_name}/n.csv', n, delimiter=',')
    np.savetxt(f'{game}/mc_data/{folder_name}/b.csv', b, delimiter=',')


def get_game():
    game = input('Enter "T" for tapatan or "P" for picaria: ')
    if game.lower() == 'p':
        game = 'picaria'
    else:
        game = 'tapatan'
    return game


def run_mc():
    game = get_game()
    # Opening saved graph
    g: Graph = load(f'{game}/{game}.graph')
    # Sorting game states into matrix indices
    # Major sorted by pieces, minor by number
    vertices = sorted(g.vertices(), key=lambda u: hash(u))
    vertices = sorted(vertices, key=lambda u: u.pieces())
    # vertices = sorted(vertices, key=lambda u: u.winner())
    # Strategies
    p1_moves = get_moves('1', game, vertices)
    p2_moves = get_moves('2', game, vertices)
    # math things to optimize later calculations
    a = aslinearoperator(transpose(bsr_matrix(p1_moves) + bsr_matrix(p2_moves)))
    # INITIAL STATE
    start = np.array([1] + [0] * (a.shape[1] - 1))
    current_state, lim_epsilon = markov_chain(start, a)
    # hashing for faster access later
    key = {v: i for i, v in enumerate(vertices)}
    # filters out non-absorbing states
    probable_vertices = [u for u in vertices if current_state[key[u]] > sys.float_info.epsilon]
    # Uncomment the following loop to see the probabilities of each states. States not shown have 0 probability.
    # for v in probable_vertices:
    #     p = current_state[key[v]]
    #     print(f'{repr(v)} pieces={v.pieces()} winner={v.winner()} probability={p}')
    # adds the probabilities of all win states for each player
    print(f'p1 win chance={sum(current_state[key[u]] for u in probable_vertices if u.winner() == "1")}')
    print(f'p2 win chance={sum(current_state[key[u]] for u in probable_vertices if u.winner() == "2")}')
    # if there is a limit to the markov chain
    if lim_epsilon <= sys.float_info.epsilon:
        print(f'limit found')
    else:
        print(f'no limit')
        period = [current_state, a.matvec(current_state)]
        diff = np.linalg.norm(period[-1] - period[0])
        while diff > sys.float_info.epsilon:
            next_state = a.matvec(period[-1])
            diff = np.linalg.norm(next_state - period[0])
            if diff > sys.float_info.epsilon:
                period.append(next_state)
        probable_period = [u for u in vertices if any(s[key[u]] > sys.float_info.epsilon for s in period)]

        nontrivial = [v for v in probable_period if v.winner() == '0']
        h = g.subgraph(nontrivial)
        filename = input('Enter name of file to save cycles to or enter "quit" to quit: ')
        if filename.lower() != 'quit':
            create_html(h, filename + '.html')


def get_moves(player, game, vertices):
    strategies = ['random', 'minimax', 'multiminimax']
    print('Available Strategies:')
    for i, strategy in enumerate(strategies):
        print(f'{i + 1}) {strategy}')
    strategy = input(f"Enter player {player}'s strategy: ")
    if strategy.lower() == strategies[1]:
        g: Graph = load(f'{game}/{game}.graph')
        depth = int(input(f"Enter player {player}'s search depth: "))
        evaluation_function = input('Enter "DEFAULT" for default, "RANDOM" for random win rate, or "2 2" for 2 2 '
                                    f'win rate evaluation function for player {player}: ')
        if evaluation_function.lower() in ['random', '2 2']:
            if evaluation_function.lower() == 'random':
                win_rates = load(f'{game}/win_rates/random_win_rates.dict')
            elif evaluation_function.lower() == '2 2':
                win_rates = load(f'{game}/win_rates/2_2_win_rates.dict')
            moves = minimax(g, vertices, player, depth=depth, eval_fn=lambda s: win_rates[s][0])
        else:
            moves = minimax(g, vertices, player, depth=depth)
    elif strategy.lower() == strategies[2]:
        g: Graph = load(f'{game}/best_moves.graph')
        moves = random(g, vertices, player)
    else:
        g: Graph = load(f'{game}/{game}.graph')
        moves = random(g, vertices, player)
    return moves


def minimax_del_edges(g: Graph, depth: int):
    if depth < 1:
        raise ValueError('Depth must be positive integer.')

    @cache
    def value(s: GameState, p: str, lvl: int):
        def evaluation(t: GameState, q: str):
            if t.winner() == q:
                return 1
            elif t.winner() == '0':
                return 0
            else:
                return -1

        if lvl == depth:
            return evaluation(s, p)
        elif lvl % 2 == 1:
            return min(value(t, p, lvl + 1) for t in g.neighbors(s))
        else:
            return max(value(t, p, lvl + 1) for t in g.neighbors(s))

    for u in g.vertices():
        if len(g.neighbors(u)) > 1:
            neighbors = {v: value(v, u.player(), 1) for v in g.neighbors(u)}
            max_value = max(neighbors.values())
            for v in neighbors:
                if neighbors[v] < max_value:
                    g.remove_edge(u, v)


def count_moves(transition_matrix: spmatrix, n_transient: int, threshold=0.05, max_moves=1000):
    a = transition_matrix.transpose()
    dot_filter = bsr_matrix(np.array([1] * n_transient + [0] * (a.shape[0] - n_transient)))
    result = []
    for i in range(a.shape[1]):
        state = a.getcol(i)
        count = 0
        while (dot_filter * state).sum() >= threshold and count < max_moves:
            state = a * state
            count += 1
        result.append(count)
    return result


def minimax_prune(graph: Graph, root: GameState, depth: int):
    if depth < 1:
        raise ValueError('Depth must be positive integer.')

    g = deepcopy(graph)

    @cache
    def value(s: GameState, p: str, lvl: int):
        def evaluation(t: GameState, q: str):
            if t.winner() == q:
                return 1
            elif t.winner() == '0':
                return 0
            else:
                return -1

        if lvl == depth:
            return evaluation(s, p)
        elif lvl % 2 == 1:
            return min(value(t, p, lvl + 1) if s.winner() == '0' else evaluation(t, p) for t in g.neighbors(s))
        else:
            return max(value(t, p, lvl + 1) if s.winner() == '0' else evaluation(t, p) for t in g.neighbors(s))

    for u in g.vertices():
        neighbors = {v: value(v, u.player(), 1) for v in g.neighbors(u)}
        max_value = max(neighbors.values())
        for v in neighbors:
            if neighbors[v] < max_value:
                g.remove_edge(u, v)

    h = Graph()
    visited = set()

    fringe = [root]
    while len(fringe) > 0:
        current_state = fringe.pop(0)
        if current_state not in visited:
            visited.add(current_state)
            h.add_vertex(current_state)
            for next_state in g.neighbors(current_state):
                h.add_edge(current_state, next_state)
                if next_state not in visited:
                    fringe.append(next_state)

    return h


def generate_win_rates(g, p1_depth, p2_depth, vertices):
    if p1_depth == 0:
        p1_moves = random(g, vertices, '1')
    else:
        p1_moves = minimax(g, vertices, '1', depth=p1_depth)
    if p2_depth == 0:
        p2_moves = random(g, vertices, '2')
    else:
        p2_moves = minimax(g, vertices, '2', depth=p2_depth)
    transition_matrix = np.array(p1_moves) + np.array(p2_moves)
    a = aslinearoperator(transpose(bsr_matrix(transition_matrix)))
    win_rates = {}
    for i in range(len(vertices)):
        start = np.zeros(a.shape[1])
        start[i] = 1
        current_state, lim_epsilon = markov_chain(start, a)
        key = {v: i for i, v in enumerate(vertices)}
        probable_vertices = [u for u in vertices if current_state[key[u]] > sys.float_info.epsilon]
        p1_win_rate = sum(current_state[key[u]] for u in probable_vertices if u.winner() == "1")
        p2_win_rate = sum(current_state[key[u]] for u in probable_vertices if u.winner() == "2")
        if lim_epsilon <= sys.float_info.epsilon:
            win_rates[vertices[i]] = (p1_win_rate, p2_win_rate)
            # print(f'{i} {repr(vertices[i])} {p1_win_rate=} {p2_win_rate=}')
            print(f'{round(i / len(vertices) * 100, 2)}%')
        else:
            raise ValueError('bruh')
    return win_rates


def markov_chain(start, transition, iterations=1000):
    lim_epsilon = 0
    # Runs markov chain
    for _ in range(iterations):  # the number of steps along the markov chain
        # Uncomment the next line to check how much rounding error accumulated. It should mathematically always be 1.
        # print(f'1-norm current = {np.linalg.norm(start, ord=1)}')
        next_state = transition.matvec(start)
        lim_epsilon = np.linalg.norm(next_state - start)
        # Uncomment the next line to see if there's a limiting superposition
        # print(f'2-norm diff = {lim_epsilon}')
        start = next_state
    return start, lim_epsilon


def write_indices_text(vertices, filename):
    with open(filename, 'wt') as outfile:
        for index, vertex in enumerate(vertices):
            print(f'{index} {repr(vertex)}', file=outfile)


def dump(o, filename):
    with open(filename, 'wb') as file:
        pickle.dump(o, file)


def load(filename):
    with open(filename, 'rb') as file:
        o = pickle.load(file)
    return o


def create_html(g: Graph, filename):
    h = nx.DiGraph()
    for u in g.vertices():
        h.add_node(str(u))
        for v in g.neighbors(u):
            h.add_edge(str(u), str(v))

    net = Network(notebook=True,  # to not open in internet explorer when finished lol
                  height='720px',
                  width='1280px',
                  directed=True,
                  layout=False)
    net.toggle_physics(False)
    net.toggle_stabilization(True)
    net.from_nx(h)
    net.show(filename)

    return net


def write_adjacency_list_text(g: Graph, filename: str):
    """
    :return: nothing, the txt file should be in the folder
    """
    with open(filename, 'wt') as outfile:
        for u, neighbors in g.adjacency_list():
            print('====================CURRENT=====================', file=outfile)
            print(u, file=outfile)
            print('=====================NEXT=======================', file=outfile)
            for v in neighbors:
                print(v, file=outfile)


if __name__ == '__main__':
    main()
