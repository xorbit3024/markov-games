from states.square_3_state import Square3State


class PicariaState(Square3State):
    """
    index   position
    ------------------
    0       top left
    1       top mid
    2       top right
    3       mid left
    4       mid mid
    5       mid right
    6       bot left
    7       bot mid
    8       bot right

    element piece
    ------------------
    '0'     empty
    '1'     player 1
    '2'     player 2
    """

    def __init__(self, description='0000000001'):
        """
        :param description: indices 0-2: top row,
                            indices 3-5: mid row,
                            indices 6-8: bot row,
                            index 9: player '1' or '2'
        """
        if len(description) != 10:
            raise ValueError('Invalid Picaria state description.')
        if any(piece not in ['0', '1', '2'] for piece in description[0:9]):
            raise ValueError("Piece can only be '0', '1', or '2'")
        self._board = list(description[0:9])
        if description[9] not in ['1', '2']:
            raise ValueError("Player can only be '1' or '2'")
        self._next_player = description[9]
        self._max_pieces = 6

    @staticmethod
    def _adjacent(index):
        if index == 0:
            return 1, 3, 4
        elif index == 1:
            return 0, 2, 3, 4, 5
        elif index == 2:
            return 1, 4, 5
        elif index == 3:
            return 0, 1, 4, 6, 7
        elif index == 4:
            return 0, 1, 2, 3, 5, 6, 7, 8
        elif index == 5:
            return 1, 2, 4, 7, 8
        elif index == 6:
            return 3, 4, 7
        elif index == 7:
            return 3, 4, 5, 6, 8
        elif index == 8:
            return 4, 5, 7
        else:
            raise ValueError(f'Invalid index {index}.')
