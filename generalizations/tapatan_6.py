from generalizations.polygon_6 import Polygon6State


class Tapatan6State(Polygon6State):
    def __init__(self, description='0' * 13 + '1'):
        """
             0  1  2
          11         3
        10     12     4
          9          5
            8   7  6

        :param description:
        """
        if len(description) != 14:
            raise ValueError('Invalid 6-Tapatan state description.')
        if any(piece not in ['0', '1', '2'] for piece in description[0:13]):
            raise ValueError("Piece can only be '0', '1', or '2'")
        self._board = list(description[0:13])
        if description[13] not in ['1', '2']:
            raise ValueError("Player can only be '1' or '2'")
        self._next_player = description[13]
        self._max_pieces = 6

    @staticmethod
    def _adjacent(index) -> tuple:
        if index == 0:
            return 1, 11, 12
        elif index == 1:
            return 0, 2, 12
        elif index == 2:
            return 1, 3, 12
        elif index == 3:
            return 2, 4, 12
        elif index == 4:
            return 3, 5, 12
        elif index == 5:
            return 4, 6, 12
        elif index == 6:
            return 5, 7, 12
        elif index == 7:
            return 6, 8, 12
        elif index == 8:
            return 7, 9, 12
        elif index == 9:
            return 8, 10, 12
        elif index == 10:
            return 9, 11, 12
        elif index == 11:
            return 0, 10, 12
        elif index == 12:
            return tuple(range(12))

    def next_states(self):
        states = super(Tapatan6State, self).next_states()
        if len(states) == 0:
            return {self}
        return states

