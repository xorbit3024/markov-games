from collections import Counter
from copy import deepcopy

from tapatan.taptan_state import TapatanState


class TapatanRepeatDrawState(TapatanState):
    __max_repeat: int
    __visited: Counter
    
    def __init__(self, max_repeat: int, description='0000000001'):
        super(TapatanRepeatDrawState, self).__init__(description)
        self.__max_repeat = max_repeat
        self.__visited = Counter()

    def __board_string(self):
        return ''.join(self.transfold()._board)

    def __hash__(self):
        return super(TapatanRepeatDrawState, self).__hash__()

    def __eq__(self, other):
        if super(TapatanRepeatDrawState, self).__eq__(other):
            return self.__visited == other.__visited
        return False

    def next_states(self):
        if self.winner() != '0':
            return {self}
        states = set()
        if self._next_player == '1':
            next_next_player = '2'
        else:
            next_next_player = '1'
        if self.pieces() < self._max_pieces:
            for index, piece in enumerate(self._board):
                if piece == '0':
                    new = deepcopy(self)
                    new._board[index] = self._next_player
                    new._next_player = next_next_player
                    if new.pieces() == new._max_pieces:
                        new.__visited[new.__board_string()] += 1
                    states.add(new)
        else:
            if self.__visited[self.__board_string()] >= self.__max_repeat:
                return {self}
            for index, piece in enumerate(self._board):
                if piece == self._next_player:
                    for adjacent in type(self)._adjacent(index):
                        if self._board[adjacent] == '0':
                            new = deepcopy(self)
                            new._board[adjacent] = self._next_player
                            new._board[index] = '0'
                            new._next_player = next_next_player
                            new.__visited[new.__board_string()] += 1
                            states.add(new)
        return states
