from copy import deepcopy

from states.game_state import GameState


class Tapatan5State(GameState):
    @staticmethod
    def _adjacent(index) -> tuple:
        if index == 0:
            return 1, 9, 10
        elif index == 1:
            return 0, 2, 10
        elif index == 2:
            return 1, 3, 10
        elif index == 3:
            return 2, 4, 10
        elif index == 4:
            return 3, 5, 10
        elif index == 5:
            return 4, 6, 10
        elif index == 6:
            return 5, 7, 10
        elif index == 7:
            return 6, 8, 10
        elif index == 8:
            return 7, 9, 10
        elif index == 9:
            return 0, 8, 10
        elif index == 10:
            return tuple(range(10))

    def winner(self) -> str:
        for player in ['1', '2']:
            if any((all(piece == player for piece in self._board[0:3]),
                    all(piece == player for piece in self._board[2:5]),
                    all(piece == player for piece in self._board[4:7]),
                    all(piece == player for piece in self._board[6:9]),
                    all(piece == player for piece in (self._board[8:10] + self._board[0:1])),
                    all(piece == player for piece in (self._board[0:6:5] + self._board[10:11])),
                    all(piece == player for piece in (self._board[1:7:5] + self._board[10:11])),
                    all(piece == player for piece in (self._board[2:8:5] + self._board[10:11])),
                    all(piece == player for piece in (self._board[3:9:5] + self._board[10:11])),
                    all(piece == player for piece in (self._board[4:10:5] + self._board[10:11])),
                    )):
                return player
        return '0'

    def next_states(self):
        states = super(Tapatan5State, self).next_states()
        if len(states) == 0:
            return {self}
        return states

    def __str__(self):
        return (f'Next: {self._next_player}\n'
                f'     {self._board[0]}\n'
                f'  {self._board[9]}    {self._board[1]}\n'
                f'{self._board[8]}   {self._board[10]}   {self._board[2]}\n'
                f' {self._board[7]}     {self._board[3]}\n'
                f'  {" ".join(self._board[6:3:-1])}')

    def __init__(self, description='0' * 11 + '1'):
        """
             0
          9    1
        8   10  2
         7     3
          6 5 4

        :param description:
        """
        if len(description) != 12:
            raise ValueError('Invalid 5-Tapatan state description.')
        if any(piece not in ['0', '1', '2'] for piece in description[0:11]):
            raise ValueError("Piece can only be '0', '1', or '2'")
        self._board = list(description[0:11])
        if description[11] not in ['1', '2']:
            raise ValueError("Player can only be '1' or '2'")
        self._next_player = description[11]
        self._max_pieces = 6

    def transformations(self):
        result = [self, self.__reflect_vertical()]
        for _ in range(8):
            result.append(result[-2].__rotate72())
        return result

    def __rotate72(self):
        new = deepcopy(self)
        new._board[0] = self._board[2]
        new._board[1] = self._board[3]
        new._board[2] = self._board[4]
        new._board[3] = self._board[5]
        new._board[4] = self._board[6]
        new._board[5] = self._board[7]
        new._board[6] = self._board[8]
        new._board[7] = self._board[9]
        new._board[8] = self._board[0]
        new._board[9] = self._board[1]
        return new

    def __reflect_vertical(self):
        new = deepcopy(self)
        new._board[1] = self._board[9]
        new._board[2] = self._board[8]
        new._board[3] = self._board[7]
        new._board[4] = self._board[6]
        new._board[6] = self._board[4]
        new._board[7] = self._board[3]
        new._board[8] = self._board[2]
        new._board[9] = self._board[1]
        return new
