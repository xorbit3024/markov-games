from abc import ABC
from copy import deepcopy

from states.game_state import GameState


class Polygon6State(GameState, ABC):
    def transformations(self):
        result = [self, self.__reflect_vertical()]
        for _ in range(10):
            result.append(result[-2].__rotate60())
        return result

    def __rotate60(self):
        new = deepcopy(self)
        new._board[0] = self._board[2]
        new._board[1] = self._board[3]
        new._board[2] = self._board[4]
        new._board[3] = self._board[5]
        new._board[4] = self._board[6]
        new._board[5] = self._board[7]
        new._board[6] = self._board[8]
        new._board[7] = self._board[9]
        new._board[8] = self._board[10]
        new._board[9] = self._board[11]
        new._board[10] = self._board[0]
        new._board[11] = self._board[1]
        return new

    def __reflect_vertical(self):
        new = deepcopy(self)
        new._board[0] = self._board[2]
        new._board[2] = self._board[0]
        new._board[3] = self._board[11]
        new._board[4] = self._board[10]
        new._board[5] = self._board[9]
        new._board[6] = self._board[8]
        new._board[8] = self._board[6]
        new._board[9] = self._board[5]
        new._board[10] = self._board[4]
        new._board[11] = self._board[3]
        return new

    def __str__(self):
        return (f'Next: {self._next_player}\n'
                f'  {"".join(self._board[0:3])}\n'
                f' {self._board[11]}   {self._board[3]}\n'
                f'{self._board[10]}  {self._board[12]}  {self._board[4]}\n'
                f' {self._board[9]}   {self._board[5]}\n'
                f'  {"".join(self._board[8:5:-1])}')

    def winner(self) -> str:
        for player in ['1', '2']:
            if any((all(piece == player for piece in self._board[0:3]),
                    all(piece == player for piece in self._board[2:5]),
                    all(piece == player for piece in self._board[4:7]),
                    all(piece == player for piece in self._board[6:9]),
                    all(piece == player for piece in self._board[8:11]),
                    all(piece == player for piece in (self._board[10:12] + self._board[0:1])),
                    all(piece == player for piece in (self._board[0:7:6] + self._board[12:13])),
                    all(piece == player for piece in (self._board[1:8:6] + self._board[12:13])),
                    all(piece == player for piece in (self._board[2:9:6] + self._board[12:13])),
                    all(piece == player for piece in (self._board[3:10:6] + self._board[12:13])),
                    all(piece == player for piece in (self._board[4:11:6] + self._board[12:13])),
                    all(piece == player for piece in (self._board[5:12:6] + self._board[12:13])),
                    )):
                return player
        return '0'
