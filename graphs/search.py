import inspect
from collections import Counter
from copy import copy, deepcopy

from graphs.graph import Graph
from states.game_state import GameState


def bfs(vertices: list[GameState], progress=False):
    g = Graph()
    visited = set()
    fringe = vertices
    while len(fringe) > 0:
        current_state = fringe.pop(0)
        if current_state not in visited:
            visited.add(current_state)
            g.add_vertex(current_state)
            for next_state in current_state.next_states():
                g.add_edge(current_state.transfold(), next_state.transfold())
                if next_state not in visited:
                    fringe.append(next_state)
        if progress:
            print(f'{len(fringe)=} {len(visited)=} {g.n()=} {g.m()=}')
    return g


def bfs_distances(g: Graph, u):
    result = {v: float('inf') for v in g.vertices()}
    result[u] = 0
    visited = set()
    fringe = [u]
    while len(visited) < len(g.vertices()) and len(fringe) > 0:
        current_state = fringe.pop(0)
        if current_state not in visited:
            visited.add(current_state)
            for next_state in g.neighbors(current_state):
                result[next_state] = min(result[next_state], result[current_state] + 1)
                if next_state not in visited:
                    fringe.append(next_state)
    return result


def dfs(vertices: list[GameState], progress=False):
    g = Graph()
    visited = set()

    def explore(state):
        visited.add(state)
        g.add_vertex(state)
        for next_state in state.next_states():
            g.add_edge(state.transfold(), next_state.transfold())
            if next_state not in visited:
                explore(next_state)
        if progress:
            depth = sum(1 for frame in inspect.stack() if frame.function == 'explore')
            print(f'{depth=} {len(visited)=} {g.n()=} {g.m()=}')

    for start in vertices:
        if start not in visited:
            explore(start)

    return g


def scc(g: Graph):
    vertices = list(g.vertices())
    key = {u: i for i, u in enumerate(vertices)}

    h = g.reverse()
    dfs_numbers = {'pre': [0] * len(vertices),
                   'post': [0] * len(vertices),
                   'count': 0}

    visited = set()

    def dfs_reverse(u):
        visited.add(u)
        dfs_numbers['pre'][key[u]] = dfs_numbers['count']
        dfs_numbers['count'] += 1
        for v in h.neighbors(u):
            if v not in visited:
                dfs_reverse(v)
        dfs_numbers['post'][key[u]] = dfs_numbers['count']
        dfs_numbers['count'] += 1

    for u in vertices:
        if u not in visited:
            dfs_reverse(u)

    i = deepcopy(g)

    def dfs_scc(graph, u):
        visited.add(u)
        graph.add_vertex(u)
        for v in i.neighbors(u):
            graph.add_edge(u, v)
            if v not in visited:
                dfs_scc(graph, v)

    sccs = []
    i = deepcopy(g)
    for u in sorted(vertices, key=lambda u: dfs_numbers['post'][key[u]], reverse=True):
        if u in i.vertices():
            j = Graph()
            visited.clear()
            dfs_scc(j, u)
            for w in j.vertices():
                i.remove_vertex(w)
            sccs.append(j)

    metagraph = Graph()
    for x in range(len(sccs)):
        metagraph.add_vertex(x)
        for y in range(len(sccs)):
            if y != x:
                if len(g.cut(sccs[x].vertices(), sccs[y].vertices())) > 0:
                    metagraph.add_edge(x,  y)

    return metagraph, sccs
