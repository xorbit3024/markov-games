import unittest

from graphs.graph import Graph
from graphs.search import scc


class TestSearchAlgorithms(unittest.TestCase):
    def test_scc(self):
        g = Graph({'A': {'B'},
                   'B': {'C', 'D', 'E'},
                   'C': {'F'},
                   'D': set(),
                   'E': {'B', 'F', 'G'},
                   'F': {'C', 'H'},
                   'G': {'H', 'J'},
                   'H': {'K'},
                   'I': {'G'},
                   'J': {'I'},
                   'K': {'L'},
                   'L': {'J'}})
        metagraph, sccs = scc(g)
        expected_sccs = [Graph({'A': set()}),
                         Graph({'B': {'E'}, 'E': {'B'}}),
                         Graph({'C': {'F'}, 'F': {'C'}}),
                         Graph({'D': set()}),
                         Graph({'G': {'H', 'J'}, 'H': {'K'}, 'K': {'L'}, 'L': {'J'}, 'J': {'I'}, 'I': {'G'}})]
        for component in expected_sccs:
            self.assertIn(component, sccs)
        p = {i: sccs.index(expected_sccs[i]) for i in range(len(expected_sccs))}
        expected_metagraph = Graph({p[0]: {p[1]}, p[1]: {p[2], p[3], p[4]}, p[2]: {p[4]}, p[3]: set(), p[4]: set()})
        self.assertEqual(expected_metagraph, metagraph)
