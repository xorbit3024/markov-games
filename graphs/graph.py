class Graph:
    """
    Directed Graph

    Self Loops: YES
    Parallel Edges: NO
    Vertex Data: YES
    Edge Data: NO
    """

    __adjacency_list: dict

    def __init__(self, adjacency_list=None):
        if adjacency_list is None:
            adjacency_list = {}
        self.__adjacency_list = adjacency_list

    def __repr__(self):
        return repr(self.__adjacency_list)

    def __str__(self):
        return f'|V|={self.n()} |E|={self.m()}'

    def add_vertex(self, u):
        if u not in self.__adjacency_list:
            self.__adjacency_list[u] = set()

    def add_edge(self, u, v):
        self.add_vertex(u)
        self.add_vertex(v)
        self.__adjacency_list[u].add(v)

    def remove_vertex(self, u):
        if u in self.__adjacency_list:
            for neighbors in self.__adjacency_list.values():
                if u in neighbors:
                    neighbors.remove(u)
            return self.__adjacency_list.pop(u)

    def remove_edge(self, u, v):
        if u in self.__adjacency_list and v in self.__adjacency_list[u]:
            self.__adjacency_list[u].remove(v)

    def vertices(self):
        """
        Don't use this to modify the graph.

        :return: iterable of vertices
        """
        return self.__adjacency_list.keys()

    def edges(self):
        """
        Don't use this to modify the graph.

        :return: iterable of edges as (u, v) pairs
        """
        return sum(([(u, v) for v in self.__adjacency_list[u]] for u in self.__adjacency_list), [])

    def adjacency_list(self):
        """
        Don't use this to modify the graph.

        :return: adjacency list as dict[type(vertex), set[type(vertex)]]
        """
        return self.__adjacency_list

    def adjacency_matrix(self, key: dict):
        """
        Don't use this to modify the graph.

        :param key: mapping of vertices to indices [0-n]
        :return: adjacency matrix as list[list[type(vertex)]]
        """
        if self.__adjacency_list.keys() != key.keys():
            raise ValueError('Invalid key.')
        matrix = [[0] * self.n() for _ in range(self.n())]
        for u, neighbors in self.__adjacency_list.items():
            for v in neighbors:
                matrix[key[u]][key[v]] = 1
        return matrix

    def neighbors(self, u):
        """
        Don't use this to modify the graph.

        :param u: vertex to find neighbors of
        :return: iterable of neighbors of u
        """
        if u not in self.__adjacency_list:
            raise KeyError(f'{u} is not a vertex in this graph.')
        return self.__adjacency_list[u]

    def subgraph(self, vertices):
        """
        :param vertices: Subset of vertices of this graph
        :return: Subgraph induced by vertices
        """
        new = Graph()
        for u in vertices:
            if u in self.__adjacency_list:
                new.add_vertex(u)
        for u, v in self.edges():
            if u in new.__adjacency_list and v in new.__adjacency_list:
                new.add_edge(u, v)
        return new

    def connected(self, u, v):
        if u in self.__adjacency_list:
            return v in self.__adjacency_list[u]
        return False

    def reverse(self):
        new = Graph({k: set() for k in self.__adjacency_list})
        for u, neighbors in self.__adjacency_list.items():
            for v in neighbors:
                new.add_edge(v, u)
        return new

    def cut(self, left, right):
        return [(u, v) for u, v in self.edges() if u in left and v in right]

    def n(self):
        return len(self.__adjacency_list)

    def m(self):
        return sum(len(neighbors) for neighbors in self.__adjacency_list.values())

    def degree(self, u):
        if u in self.__adjacency_list:
            return len(self.__adjacency_list[u])
        else:
            raise KeyError(f'{u} is not a vertex of this graph.')

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return self.__adjacency_list == other.__adjacency_list
        return False
